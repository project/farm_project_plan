<?php

namespace Drupal\farm_project_plan\Plugin\Plan\PlanType;

use Drupal\farm_entity\Plugin\Plan\PlanType\FarmPlanType;

/**
 * Provides the project plan type.
 *
 * @PlanType(
 *   id = "project",
 *   label = @Translation("Project"),
 * )
 */
class Project extends FarmPlanType {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    // Add DuplicateReference constraint.
    $fields['log']->addConstraint('DuplicateReference');

    // Add start and end date fields.
    $field_info = [
      'project_start' => [
        'type' => 'timestamp',
        'label' => $this->t('Start date'),
        'weight' => [
          'form' => -4,
          'view' => 5,
        ],
      ],
      'project_end' => [
        'type' => 'timestamp',
        'label' => $this->t('End date'),
        'weight' => [
          'form' => -4,
          'view' => 5,
        ],
      ],
      'project_goal' => [
        'type' => 'text_long',
        'label' => t('Goal'),
        'description' => t('A description about the goals of this project.'),
        'weight' => [
          'form' => -3,
          'view' => 6,
        ],
      ],
    ];
    foreach ($field_info as $name => $info) {
      $fields[$name] = $this->farmFieldFactory->bundleFieldDefinition($info);
    }

    return $fields;
  }

}
