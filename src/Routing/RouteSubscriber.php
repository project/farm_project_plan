<?php

namespace Drupal\farm_project_plan\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes for the farm_project_plan module.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    // Only display the "Logs" tab for project plans.
    if ($route = $collection->get('view.project_logs.page')) {
      // Add to existing parameters, if any.
      $parameters = $route->getOption('parameters');
      $parameters['plan'] = [
        'type' => 'entity:plan',
        'bundle' => ['project'],
      ];
      $route->setOption('parameters', $parameters);
    }
  }

}
