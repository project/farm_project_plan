# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.1.1 2024-03-29

### Fixed

- Fix project logs tab being visible on other plan types due to use of deprecated route access restriction.

## 1.1.0 2023-11-02

### Added

- Support for Drupal 10 and farmOS v3.

## 1.0.0 2023-03-17

### Added

- Add project plan type.
- Add log action to add logs to a plan.
- Add tab with view of project logs.
- Add planned status for project plans.
- Add fields for start and end date.
- Add text field for the project goal.
- Add tabs for planned, active and archived plans.
